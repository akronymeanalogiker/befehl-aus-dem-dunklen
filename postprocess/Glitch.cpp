#include "Glitch.h"

#include "schwubbel_config.h"

#include <algorithm>
#include <cmath>

// swap area
// LeColor* data = &((LeColor*) bitmap->data)[96*bitmap->tx];
// for (int y=0;y<32;y++) {
//     uint32_t* xdata = data + 70;
//     for (int x=0;x<32;x++) {
//         std::swap(*(xdata+32), *xdata);
//         xdata++;
//     }
//     data += bitmap->tx;
// }

void Glitch::glitch(unsigned long duration, LeBitmap* bitmap) {
    float dsin = sin(duration);
    int rshift = dsin * 16.f;
    int gshift = dsin * 8.f;

    LeColor* s = (LeColor*) bitmap->data;
    LeColor* d = (LeColor*) bitmap->data;
    int bitmapSize = bitmap->tx * bitmap->ty;
    for (int i=0;i<bitmapSize;i++) {
        int rsrc = i + rshift;
        if (rsrc >= 0 && rsrc < bitmapSize) {
            d->r = s[rsrc].r;
        }
        int gsrc = i + gshift;
        if (gsrc >= 0 && gsrc < bitmapSize) {
            d->g = s[gsrc].g;
        }
        d++;
    }
}