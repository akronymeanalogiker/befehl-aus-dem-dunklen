#pragma once

#include "le3d/engine/bitmap.h"

class Glitch {
public:
    void glitch(unsigned long, LeBitmap*);
};