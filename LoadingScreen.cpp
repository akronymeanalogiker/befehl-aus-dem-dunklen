#include "LoadingScreen.h"

#include "schwubbel_config.h"
#include "schwubbel_support.h"

#include "le3d/engine/le3d.h"

#ifdef _MSC_VER
	#include "le3d/engine/vs/vs-dirent.h"
#elif defined(__WATCOMC__)
	#include <direct.h>
#else
	#include <dirent.h>
#endif

#include <vector>

LoadingScreen::LoadingScreen(LeRasterizer* rasterizer, LeWindow* window, LeDraw* draw):
    rasterizer(rasterizer),
    window(window),
    draw(draw)
{
    tom = bmpCache.loadBMP("assets/tom.bmp");
    preKalk = bmpCache.loadBMP("assets/prekalk.bmp");

    rasterizer->flush();
    rasterizer->frame.blit(SCHWUBBEL_WIDTH/2 - preKalk->tx/2, SCHWUBBEL_HEIGHT/2, preKalk, 0, 0, preKalk->tx, preKalk->ty);

}

void LoadingScreen::load() {
    char ext[LE_MAX_FILE_EXTENSION+1];
	char filePath[LE_MAX_FILE_PATH+1];

    const char* path = "assets";
	DIR * dir = opendir(path);
	struct dirent * dd;
    

    std::vector<LoadingItem> items;
	while ((dd = readdir(dir))) {
		if (dd->d_name[0] == '.') continue;
		LeGlobal::getFileExtention(ext, LE_MAX_FILE_EXTENSION, (const char*) dd->d_name);

        snprintf(filePath, LE_MAX_FILE_PATH, "%s/%s", path, dd->d_name);
        filePath[LE_MAX_FILE_PATH] = '\0';
		if (strcmp(ext, "bmp") == 0) {
            auto it = items.begin();
		// Load a Windows bmp file
            items.insert(it, {
                LOADING_TYPE_BMP,
                std::string(filePath)
            });
        } else if (strcmp(ext, "obj") == 0) {
            items.push_back({
                LOADING_TYPE_OBJ,
                std::string(filePath)
            });
        }
	}
	closedir(dir);

    size_t len = items.size();
    update(0, len);

    for (int i=0;i<len;i++) {
        if (items[i].type == LOADING_TYPE_BMP) {
            printf("bmpCache: loading bitmap: %s\n", items[i].path.c_str());
            bmpCache.loadBMP(items[i].path.c_str());
        } else if (items[i].type == LOADING_TYPE_OBJ) {
            printf("meshCache: loading obj: %s\n", items[i].path.c_str());
            meshCache.loadOBJ(items[i].path.c_str());
        }
        update(i+1, len);
    }
}

void LoadingScreen::update(int numDone, int numItems) {
#ifdef AMIGA
    rasterizer->frame.alphaBlit((SCHWUBBEL_WIDTH - 40)/numItems*numDone + 20, (SCHWUBBEL_HEIGHT) / 2 - tom->ty, tom, 0, 0, tom->tx, tom->ty);
    draw->setPixels(rasterizer->getPixels());
#endif
}