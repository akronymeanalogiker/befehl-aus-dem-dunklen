#pragma once

void init_timing();
void cleanup_timing();
void wait_next_frame();
unsigned long millis();