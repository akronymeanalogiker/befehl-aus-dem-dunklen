#pragma once

#include "le3d/engine/bitmap.h"
#include "le3d/engine/rasterizer.h"

#include <string>

#define LOADING_TYPE_BMP 1
#define LOADING_TYPE_OBJ 2

struct LoadingItem {
    int type;
    std::string path;
};

struct LoadingScreen {
public:
    LoadingScreen(LeRasterizer*, LeWindow*, LeDraw*);
    void load();
    void update(int, int);

private:
    LeRasterizer* rasterizer;
    LeDraw* draw;
    LeWindow* window;

    LeBitmap* bitmap;
    LeBitmap* tom;
    LeBitmap* preKalk;
};