#include "schwubbel_support.h"

#include <stdint.h>
#include <stdio.h>

#ifdef _WIN32
#include <windows.h>
#else
#include <time.h>
#endif

#ifdef AMIGA
#include <proto/exec.h>
#include <proto/timer.h>

struct timerequest* timereq = NULL;
struct MsgPort *port = NULL;
struct EClockVal ecval;
struct Device* TimerBase = NULL;
uint32_t E_Freq;
#endif

void init_timing() {
#ifdef AMIGA
	port = CreateMsgPort();
	if (!port) {
		printf("Couldn't create message port!\n");
		return;
	}
	timereq = (timerequest*) AllocMem(sizeof(timerequest), MEMF_CLEAR|MEMF_PUBLIC);
	if (!timereq) {
		printf("Couldn't alloc timerequest!\n");
		return;
	}

	uint32_t error = OpenDevice("timer.device", UNIT_WAITECLOCK, &timereq->tr_node, 0);
	if (error > 0) {
		printf("Couldn't open timer.device %d!\n", error);
		return;
	}
	TimerBase = (struct Device*) timereq->tr_node.io_Device;
	timereq->tr_node.io_Message.mn_ReplyPort = port;
	timereq->tr_node.io_Command = TR_ADDREQUEST;

    E_Freq = ReadEClock(&ecval) / 1000;
#endif
}

void cleanup_timing() {
#ifdef AMIGA
    if (timereq) {
		CloseDevice(&timereq->tr_node);
		FreeMem(timereq, sizeof(timerequest));
	}
	if (port) {
		DeleteMsgPort(port);
	}
#endif
}

void wait_next_frame() {
    static uint8_t frames = 0;
    static unsigned long lastMillis;

	unsigned long diff;
	unsigned long currentMillis;
	do {
    	currentMillis = millis();
		diff = currentMillis - lastMillis;
	} while (diff == 0);
	
    if (++frames == 100) {
		printf("FPS: %d\n", (int) (1000 / (currentMillis - lastMillis)));
        frames = 0;
    }
    lastMillis = currentMillis;
	// vblank doesn't really work on vampire? or I just don't get it...
	// feeding constant updates doesn't break anything so far so stick to it :)
}

unsigned long millis() {
#if AMIGA
    ReadEClock(&ecval);
    int64_t pc = ((int64_t) ecval.ev_hi << 32) | ecval.ev_lo;
	return pc / E_Freq;
#elif _WIN32
    static LARGE_INTEGER s_frequency;
    static BOOL s_use_qpc = QueryPerformanceFrequency(&s_frequency);
    if (s_use_qpc) {
        LARGE_INTEGER now;
        QueryPerformanceCounter(&now);
        return (1000LL * now.QuadPart) / s_frequency.QuadPart;
	}
	return 0;
#else
	struct timespec tp;
	clock_gettime(CLOCK_MONOTONIC_RAW, &tp);

    return tp.tv_sec * 1000 + tp.tv_nsec / 1000000;
#endif
}