#include "DebugWindow.h"

#include "stdio.h"

#include "schwubbel_config.h"

#include "le3d/engine/bmpfile.h"
#include "le3d/engine/window.h"

DebugWindow::DebugWindow(uint16_t const& width, uint16_t const& height):
    settings(),
    width(width),
    height(height),
    swOptions()
{
    ImGui::CreateContext();
    ImGuiIO& io = ImGui::GetIO();

    bitmap.allocate(width, height + 2);
    bitmap.flags = LE_BITMAP_PREMULTIPLIED;
    io.DisplaySize = ImVec2(width, height);
    imgui_sw::bind_imgui_painting();

}

DebugWindow::~DebugWindow() {
    bitmap.deallocate();
    ImGui::DestroyContext();
}

DebugSettings& DebugWindow::update() {
    bitmap.clear(LeColor(0, 0, 0, 0xff));

    ImGui::NewFrame();
    ImGui::Begin("Debug");
    
    ImGui::RadioButton("Full demo", (int*) &settings.debugScene, (int) DebugScene::FULL_DEMO);
    ImGui::RadioButton("Scroller", (int*) &settings.debugScene, (int) DebugScene::SCROLLER_SCENE);
    ImGui::RadioButton("Skytree", (int*) &settings.debugScene, (int) DebugScene::SKYTREE_SCENE);
    ImGui::RadioButton("City", (int*) &settings.debugScene, (int) DebugScene::CITY_SCENE);
    ImGui::RadioButton("Credits", (int*) &settings.debugScene, (int) DebugScene::CREDITS_SCENE);
    ImGui::RadioButton("Outro", (int*) &settings.debugScene, (int) DebugScene::OUTRO_SCENE);
    
    ImGui::SliderFloat("cityViewPos.x", (float*) &settings.cityViewPosition.x, -10.0f, 10.0f);
    ImGui::SliderFloat("cityViewPos.y", (float*) &settings.cityViewPosition.y, -10.0f, 10.0f);
    ImGui::SliderFloat("cityViewPos.z", (float*) &settings.cityViewPosition.z, -10.0f, 10.0f);

    ImGui::SliderFloat("cityViewAngle.x", (float*) &settings.cityViewAngle.x, -180.0f, 180.0f);
    ImGui::SliderFloat("cityViewAngle.y", (float*) &settings.cityViewAngle.y, -180.0f, 180.0f);
    ImGui::SliderFloat("cityViewAngle.z", (float*) &settings.cityViewAngle.z, -180.0f, 180.0f);

    ImGui::SliderInt("time", (int*) &settings.elapsed, 0, 200000);

    ImGui::End();
    ImGui::Render();
    paint_imgui((uint32_t*) bitmap.data, width, height, swOptions);

    uint32_t* data = (uint32_t*) bitmap.data;
    for (int i=0;i<width*height;i++) {
        if ((data[i] & 0x00ffffff) > 0) {
            data[i] &= 0x00ffffff;
        } 
    }
    return settings;
}

void DebugWindow::keyCallback(int key, int state) {
    if (key == 61) {
        sys.terminate();
    }
}

void DebugWindow::mouseCallback(int x, int y, int buttons) {
    ImGuiIO& io = ImGui::GetIO();
    io.MousePos = ImVec2((float) x, (float) y);
    io.MouseDown[0] = buttons & LE_WINDOW_MOUSE_LEFT;
    io.MouseDown[1] = buttons & LE_WINDOW_MOUSE_RIGHT;
    io.MouseDown[2] = buttons & LE_WINDOW_MOUSE_MIDDLE;
}