#include "MathHelpers.h"

#include <stdio.h>

LeMatrix MathHelpers::createLookAtMatrix(LeVertex const& position, LeVertex const& target) {
    LeVertex z = position-target;
    z.normalize();

    LeVertex x = LePrimitives::up.cross(z);
    x.normalize();

    LeVertex y = z.cross(x);
    
    LeMatrix translation;
    translation.mat[0][3] = -position.x;
    translation.mat[1][3] = -position.y;
    translation.mat[2][3] = -position.z;

    LeMatrix rotation;
    rotation.mat[0][0] = x.x;
    rotation.mat[0][1] = x.y;
    rotation.mat[0][2] = x.z;
    rotation.mat[1][0] = y.x;
    rotation.mat[1][1] = y.y;
    rotation.mat[1][2] = y.z;
    rotation.mat[2][0] = z.x;
    rotation.mat[2][1] = z.y;
    rotation.mat[2][2] = z.z;

    return rotation*translation;
}

LeVertex MathHelpers::cubicCurvePosition(BezierSegment const& segment, float t) {
    LeVertex start = { segment.sx, segment.sy, segment.sz };
    LeVertex end = { segment.ex, segment.ey, segment.ez };
    LeVertex control1 = { segment.c1x, segment.c1y, segment.c1z };
    LeVertex control2 = { segment.c2x, segment.c2y, segment.c2z };
    float u = 1.0f-t;
    float uu = u*u;
    float uuu = uu*u;

    float tt = t*t;
    float ttt = tt*t;

    LeVertex result = start * uuu;
    result += control1*3.f*uu*t;
    result += control2*3.f*u*tt;
    result += end*ttt;

    return result;
}