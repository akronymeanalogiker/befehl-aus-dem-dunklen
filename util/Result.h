#pragma once

#include <string>

template <typename T>
struct Result {
    T v;
    std::string err;
};