#pragma once

#include "BezierSegment.h"

#include "Result.h"

#include <string>
#include <vector>

struct BezierPathParser {
public:
    static Result<std::vector<BezierSegment>> parse(std::string const&);
    static Result<std::vector<BezierSegment>> parseFile(std::string const&);
};