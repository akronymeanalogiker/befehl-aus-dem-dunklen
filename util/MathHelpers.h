#pragma once

#include "le3d/engine/geometry.h"
#include "BezierSegment.h"

struct MathHelpers {
public:
    static LeMatrix createLookAtMatrix(LeVertex const& position, LeVertex const& target);
    static LeVertex cubicCurvePosition(BezierSegment const& segment, float t);
};