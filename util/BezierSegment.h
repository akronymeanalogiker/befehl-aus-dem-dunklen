#pragma once

struct BezierSegment {
    float sx;
    float sy;
    float sz;
    float c1x;
    float c1y;
    float c1z;
    float c2x;
    float c2y;
    float c2z;
    float ex;
    float ey;
    float ez;
    float len;
};