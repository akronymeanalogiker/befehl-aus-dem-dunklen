#pragma once

#include "le3d/engine/bitmap.h"

#include <stdlib.h>

struct BitmapHelpers {
public:
    static void darken(LeBitmap*, uint8_t);
};