#include "BezierPathParser.h"

#include "picojson/picojson.h"

#include <string>
#include <stdio.h>
#include <stdlib.h>

using namespace picojson;

Result<std::vector<BezierSegment>> BezierPathParser::parse(std::string const& json) {
    std::vector<BezierSegment> result;

    picojson::value v;
    std::string err = picojson::parse(v, json);
    if (!err.empty()) {
        char errStr[1024];
        int n = snprintf(errStr, 1024, "Error parsing json: %s\n", err.c_str());
        return {
            result,
            std::string(errStr, n)
        };
    }
    
    array arr = v.get<array>();
    array::iterator it;
    for (it = arr.begin(); it != arr.end(); it++) {
        object obj = it->get<object>();
        result.push_back({
            // beware y and z flipped!
            (float) obj["sx"].get<double>(),
            (float) obj["sz"].get<double>(),
            (float) -obj["sy"].get<double>(),
            (float) obj["c1x"].get<double>(),
            (float) obj["c1z"].get<double>(),
            (float) -obj["c1y"].get<double>(),
            (float) obj["c2x"].get<double>(),
            (float) obj["c2z"].get<double>(),
            (float) -obj["c2y"].get<double>(),
            (float) obj["ex"].get<double>(),
            (float) obj["ez"].get<double>(),
            (float) -obj["ey"].get<double>(),
            (float) obj["len"].get<double>(),
        });
    }

    return {
        result,
        std::string()
    };
}

Result<std::vector<BezierSegment>> BezierPathParser::parseFile(std::string const& path) {
    FILE * file = fopen(path.c_str(), "rb");
	if (!file) {
        char errStr[1024];
        int n = snprintf(errStr, 1024, "Error opening file: %s\n", path.c_str());
        return {
            {},
            std::string(errStr, n)
        };
	}

    fseek(file, 0, SEEK_END);
    int size = ftell(file);
    rewind(file);

    char* buffer = (char*) malloc(size * sizeof(char));
    fread(buffer, sizeof(char), size, file);
    std::string json(buffer, size);
    free(buffer);

    return parse(json);
}