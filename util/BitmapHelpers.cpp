#include "BitmapHelpers.h"

void BitmapHelpers::darken(LeBitmap* bitmap, uint8_t factor) {
    int total = bitmap->tx*bitmap->ty;
    LeColor* data = (LeColor*) bitmap->data;
    for (int i=0;i<total;i++) {
        data->r = (data->r * factor) >> 8;
        data->g = (data->g * factor) >> 8;
        data->b = (data->b * factor) >> 8;
        data++;
    }
}