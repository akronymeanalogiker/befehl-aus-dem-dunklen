#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

#include "le3d/engine/le3d.h"

class Scene {
public:
    virtual void start() {};
    virtual void stop() {};
    virtual void run(unsigned long const&) = 0;
    virtual void postprocess() {};

    virtual ~Scene() {};
};