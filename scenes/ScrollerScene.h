#pragma once

#include "Scene.h"

#include "le3d/engine/rasterizer.h"
#include "le3d/engine/renderer.h"
#include "le3d/engine/bitmap.h"

#include "postprocess/Glitch.h"
#include "schwubbel_config.h"

class ScrollerScene: public Scene {
public:
    ScrollerScene(LeRenderer*, LeRasterizer*, LeMesh*);
    
public:
    virtual void start() override;
    virtual void stop() override;
    virtual void run(unsigned long const&) override;

private:
    static constexpr int32_t numStars = 255;
    static constexpr int32_t starCanvasHeight = 160;
    static constexpr int32_t barHeight = 10;
    static constexpr int32_t starCanvasStart() { return barHeight; }
    static constexpr uint16_t fontBitmapSize = 256;
    static constexpr uint8_t fontCharSize = 32;

private:
    void generateStars();
    void renderStarField(unsigned long const&);
    void renderScroller(unsigned long const&);
    const char* renderNextChar(const char*, uint16_t const&, uint16_t const&, uint8_t const&, uint8_t const&);
    LeVertex quadraticBezierCurve(float const& t, LeVertex const& start, LeVertex const& end, LeVertex const& control);

private:
    struct StarLocation {
        uint16_t x;
        uint16_t y;
        uint16_t z;
    };
    StarLocation stars[numStars];
    LeRenderer* renderer;
    LeRasterizer* rasterizer;

    LeMesh* vampire;
    LeBitmap* logo;
    LeBitmap* fontBitmap;
    LeBitmap* schnappsGirls;

    uint16_t scrollerLength;
    uint16_t scrollerPixelLength;
    Glitch glitch;
};