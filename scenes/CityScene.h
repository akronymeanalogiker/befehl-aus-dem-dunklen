#pragma once

#include "Scene.h"

#include "le3d/engine/bitmap.h"
#include "le3d/engine/geometry.h"
#include "le3d/engine/mesh.h"
#include "le3d/engine/rasterizer.h"
#include "le3d/engine/renderer.h"
#include "util/BezierSegment.h"

#include <memory>

#define STOMP_START_TIME 12500
#define STOMP_TIME 100

#define SHATTER_TIME 300

#define LEAVE_START_TIME 18000
#define LEAVE_TIME 400

#define ZOOM_START_TIME 22500
#define ZOOM_TIME 4000

class CityScene: public Scene {
public:
    CityScene(LeRenderer*, LeRasterizer*, BezierSegment const&);
    ~CityScene();
    
    void setViewPosition(LeVertex const& viewPosition) {
        return;
        this->viewPosition = viewPosition;
        renderer->setViewPosition(this->viewPosition);
        renderer->updateViewMatrix();
    }

    void setViewAngle(LeVertex const& viewAngle) {
        return;
        this->viewAngle = viewAngle;
        renderer->setViewAngle(this->viewAngle);
        renderer->updateViewMatrix();
    }

    LeVertex const& getViewPosition() const {
        return viewPosition;
    }

    LeVertex const& getViewAngle() const {
        return viewAngle;
    }

    virtual void start() override;
    virtual void postprocess() override;
    virtual void run(unsigned long const&) override;

private:
    void renderEnvironment(unsigned long const& elapsed);
    void renderBackground();

private:
    LeRenderer* renderer;
    LeRasterizer* rasterizer;

    BezierSegment bloodPath;

    bool hasFinal;
    
    LeBitmap* bg;
    LeBitmap* compoIntermediateFinal;
    LeBitmap rendereredBackground;

    LeMesh* city;
    LeMesh* vampire;
    LeMesh* godzilla;
    LeMesh* street;
    LeMesh* compointermediate;

    LeVertex viewPosition;
    LeVertex viewAngle;
    LeVertex startLookAt;
    LeVertex endLookAt;
};