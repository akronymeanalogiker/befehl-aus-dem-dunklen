#include "SkyTreeScene.h"

#include <algorithm>

#include "schwubbel_config.h"

SkyTreeScene::SkyTreeScene(LeRenderer* renderer, LeRasterizer* rasterizer, LeMesh* vampire) :
    renderer(renderer),
    rasterizer(rasterizer),
    vampireMatrix(),
    vampire(vampire),
    cameraPosition(0.0f, 0.0f, 1.5f)
{
    int skyTreeSlot = meshCache.getSlotFromName("skytree.obj");
	skyTree = meshCache.cacheSlots[skyTreeSlot].mesh;

    int flagSlot = meshCache.getSlotFromName("flag.obj");
	flag = meshCache.cacheSlots[flagSlot].mesh;

    int bgSlot = bmpCache.getSlotFromName("bg.bmp");
	bg = bmpCache.cacheSlots[bgSlot].bitmap;
}

void SkyTreeScene::start() {
    renderer->setViewAngle(LeVertex(0.0f, 0.0f, 0.0f));
    renderer->setViewPosition(cameraPosition);
	renderer->updateViewMatrix();

    flag->pos.y = 16.9f;
    flag->pos.x = 1.3f;
    flag->updateMatrix();

    vampire->angle.z = -45.0f;
    vampire->scale.x = 0.2f;
    vampire->scale.y = 0.2f;
    vampire->scale.z = 0.2f;

    skyTree->angle.y = 90.0f;
    skyTree->updateMatrix();
}

void SkyTreeScene::run(unsigned long const& elapsed) {
    memcpy(rasterizer->frame.data, bg->data, bg->tx * bg->ty * sizeof(LeColor));

    float y = elapsed / 1500.f + 1.5f;

    float cameraY = std::min(17.0f, y);
    cameraPosition = LeVertex(0.0f, cameraY, 3.0f);
    renderer->setViewPosition(cameraPosition);
	renderer->updateViewMatrix();
    renderer->render(skyTree);

    float xAngle = elapsed / 2000.0f;
    float zAngle = elapsed / 2000.0f;
    float x = sinf(xAngle) * 2.0f;
    float z = cosf(zAngle) * 2.0f;

    vampireMatrix.identity();
    vampireMatrix.rotateZ(-M_PI_4);
    vampireMatrix.rotateY(xAngle + M_PI_2);
    vampireMatrix.scale(LeVertex(0.2f, 0.2f, 0.2f));
    vampireMatrix.translate(LeVertex(x, y - 0.2f, z));

    vampire->setMatrix(vampireMatrix);

    renderer->setBackculling(false);
    renderer->render(vampire);
    renderer->setBackculling(true);

    float wave1 = sinf(elapsed / 1000.0f) * 0.2f;
    float wave2 = wave1 * -1;
    flag->vertexes[2].z = wave1;
    flag->vertexes[3].z = wave1;
    flag->vertexes[4].z = wave2;
    flag->vertexes[5].z = wave1;
    flag->vertexes[6].z = wave1;
    flag->vertexes[7].z = wave2;
    // flag->angle = LeVertex(0.0f, 1.0f, 1.0f);

    renderer->render(flag);
}