#pragma once

#define _USE_MATH_DEFINES
#include <cmath>

#include "Scene.h"

#include "le3d/engine/bitmap.h"
#include "le3d/engine/geometry.h"
#include "le3d/engine/mesh.h"
#include "le3d/engine/rasterizer.h"
#include "le3d/engine/renderer.h"

#include <memory>

class SkyTreeScene: public Scene {
private:

public:
    SkyTreeScene(LeRenderer*, LeRasterizer*, LeMesh*);
    virtual void start() override;
    virtual void run(unsigned long const&) override;

private:
    LeRenderer* renderer;
    LeRasterizer* rasterizer;
    LeVertex cameraPosition;

    LeMatrix vampireMatrix;
    LeMesh* vampire;
    LeMesh* skyTree;
    LeMesh* flag;
    LeBitmap* bg;
};