#include "CreditsScene.h"

#include "util/BitmapHelpers.h"
#include "util/MathHelpers.h"

#include <algorithm>

#define dist(a, b, c, d) sqrt(double((a - c) * (a - c) + (b - d) * (b - d)))

CreditsScene::CreditsScene(LeRenderer* renderer, LeRasterizer* rasterizer, std::vector<BezierSegment> cameraPath[NUM_CREDITS_PATHS]) :
    renderer(renderer),
    rasterizer(rasterizer),
    cameraPath {cameraPath[0], cameraPath[1], cameraPath[2], cameraPath[3]},
    cameraPathLength(),
    durationPerPath(10000),
    fadeFactor(-1.f),
    mop(nullptr),
    hoshi(nullptr),
    virgill(nullptr),
    plasma(),
    pathIndex(0)
{
    for (int i=0;i<NUM_CREDITS_PATHS;i++) {
        for (auto it=cameraPath[i].begin();it!=cameraPath[i].end();it++) {
            cameraPathLength[i] += it->len;
        }
        cameraPathPerMs[i] = cameraPathLength[i] / durationPerPath;
    }

    int slot;
    slot = meshCache.getSlotFromName("egg.obj");
	egg = meshCache.cacheSlots[slot].mesh;

    slot = meshCache.getSlotFromName("landscape.obj");
	plane = meshCache.cacheSlots[slot].mesh;

    slot = bmpCache.getSlotFromName("landscape.bmp");
	planeTexture = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("mop.bmp");
    mop = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("hoshi.bmp");
    hoshi = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("virgill.bmp");
    virgill = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("bg.bmp");
	bg = bmpCache.cacheSlots[slot].bitmap;

    // for (int i=0;i<255;i++) {
    //     uint8_t r = (uint8_t) (128.f + sinf(M_PI * i / 7.f) * 128.f);
    //     uint8_t g = (uint8_t) (128.f + sinf(M_PI * i / 8.f) * 128.f);
    //     uint8_t b = (uint8_t) (128.f + sinf(M_PI * i / 4096.f) * 128.f);

    //     palette[i] = LeColor(r, g, b, 0);
    // }

    // plasma.allocate(64, 64);
    // plasma.flags = LE_BITMAP_PREMULTIPLIED;
    // plasmaTextureSlot = bmpCache.createSlot(&plasma, "plasma.bmp");

    // slot = meshCache.getSlotFromName("plasmaplane.obj");
    // plasmaPlane = meshCache.cacheSlots[slot].mesh;
    
    // plasmaPlane->texSlotList[0] = plasmaTextureSlot;
    // plasmaPlane->texSlotList[1] = plasmaTextureSlot;
}

CreditsScene::~CreditsScene() {
    // to prevent double free
    // bmpCache.cacheSlots[plasmaTextureSlot].bitmap = NULL;
    // bmpCache.deleteSlot(plasmaTextureSlot);
    // plasma.deallocate();
}

void CreditsScene::start() {
}

void CreditsScene::stop() {
}

void CreditsScene::updateCamera(BezierSegment const& segment, float t) {
    LeVertex cameraPosition = MathHelpers::cubicCurvePosition(segment, t);
    LeVertex center(0.f, 0.5f, 0.f);

    renderer->setViewMatrix(MathHelpers::createLookAtMatrix(cameraPosition, center));
}

void CreditsScene::run(unsigned long const& elapsed) {
    rasterizer->frame.blit(0, 0, bg, 0, 0, bg->tx, bg->ty);
    
    int clampedElapsed = elapsed % (durationPerPath*NUM_CREDITS_PATHS);
    pathIndex = clampedElapsed / durationPerPath;

    std::vector<BezierSegment> const& currentCameraPath = cameraPath[pathIndex];
    float finishedSegmentsLength = 0.f;
    float currentPos = (clampedElapsed - pathIndex * durationPerPath) * cameraPathPerMs[pathIndex];
    float currentCameraPathLength = cameraPathLength[pathIndex];
    if (currentPos > currentCameraPathLength) {
        currentPos = currentCameraPathLength - 0.01;
    }

    float t = 0.f;
    std::vector<BezierSegment>::const_iterator it;
    for (it=currentCameraPath.begin();it!=currentCameraPath.end();it++) {
        if (finishedSegmentsLength + it->len > currentPos) {
            t = (currentPos-finishedSegmentsLength)/it->len;
            updateCamera(*it, t);
            break;
        }
        finishedSegmentsLength += it->len;
    }

    renderer->render(plane);
    renderer->render(egg);

    if (pathIndex == 3) {
        // float time = (float) elapsed;
        // // much copy pasta
        // // https://lodev.org/cgtutor/plasma.html
        // LeColor* bitmapData = (LeColor*) plasma.data;
        // for (int y=0;y<plasma.ty;y++) {
        //     for (int x=0;x<plasma.tx;x++) {
        //         float fx = (float) x;
        //         float fy = (float) y;

        //         float value = sin(dist(x + time / 509.f, y, 128.0, 128.0) / 8.0)
        //             + sin(dist(x, y, 17.0, 64.0) / 8.0)
        //             + sin(dist(x, y + time / 7., 192.0, 64) / 18.0)
        //             + sin(dist(x, y, 192.0, 100.0) / 8.0)
        //             + 27.f;
                
        //         *(bitmapData++) = palette[(uint8_t) value];
        //     }
        // }
        // renderer->render(plasmaPlane);
    }
    
    fadeFactor = -1.f;
    if (t <= 0.2f) {
        fadeFactor = t/0.2f;
    } else if (t >= 0.8f) {
        fadeFactor = (1.0f-t)/0.2f;
    }
}

void CreditsScene::postprocess() {
    LeBitmap* currentBitmap = NULL;
    if (pathIndex == 0) {
        currentBitmap = mop;
    } else if (pathIndex == 1) {
        currentBitmap = hoshi;
    } else if (pathIndex == 2) {
        currentBitmap = virgill;
    }

    if (currentBitmap != NULL) {
        rasterizer->frame.alphaBlit(rasterizer->frame.tx/2 - currentBitmap->tx/2, 20,
            currentBitmap, 0, 0, currentBitmap->tx, currentBitmap->ty);
    }

    if (fadeFactor >= 0.f) {
        BitmapHelpers::darken(&rasterizer->frame, (uint8_t) (fadeFactor*255.f));
    }
}