#include "ScrollerScene.h"

#include <stdint.h>

const char* scrollerText = "HELLO EVOKE! DO YOU LIKE AMIGA SCROLLERS?! "
    "WE KNOW YOU LOVE THEM! AND SO WE PRESENT OUR FIRST EVER AMIGA DEMO! "
    "GREETINGS FLY OUT TO: "
    "5711 * ALPHA RAID * FMS-CAT * HAUJOBB * LEVEL 90 * POO-BRAIN "
    "* PRISMBEIGNS * RIFT "
    "* SCOOPEX * SUBGROUND4 * SUNSPIRE * TRSI * T$ * XAYAX * YX       ";

ScrollerScene::ScrollerScene(LeRenderer* renderer, LeRasterizer* rasterizer, LeMesh* vampire) :
    renderer(renderer),
    rasterizer(rasterizer),
    vampire(vampire),
    stars(),
    scrollerLength(strlen(scrollerText)),
    scrollerPixelLength(scrollerLength * fontCharSize)
{
    int slot = bmpCache.getSlotFromName("akronymeanalogiker_bigger.bmp");
	logo = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("scrollerfont.bmp");
	fontBitmap = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("schnappsgirls.bmp");
    schnappsGirls  = bmpCache.cacheSlots[slot].bitmap;
    generateStars();
}

void ScrollerScene::generateStars() {
    uint32_t seed = 0xaff11871;

    for (int i=0;i<numStars;i++) {
        stars[i].x = ((uint8_t)seed) * SCHWUBBEL_WIDTH / 255;
        seed ^= seed << 24;
        seed ^= seed >> 16;
        stars[i].y = ((uint8_t)seed) * starCanvasHeight / 255;
        seed ^= seed << 12;
        seed ^= seed >> 5;

        stars[i].z = seed % 3;
    }
}

void ScrollerScene::renderStarField(unsigned long const& elapsed) {
    LeColor* frameData = (LeColor*) rasterizer->frame.data;
    for (int i=0;i<numStars;i++) {
        uint16_t x = (elapsed / (50 * (stars[i].z + 1)) + stars[i].x) % SCHWUBBEL_WIDTH;
        LeColor* pixel = &frameData[(starCanvasStart() + stars[i].y) * SCHWUBBEL_WIDTH + x];

        uint8_t starColor = 0xff;
        if (stars[i].z == 1) {
            starColor = 0xaa;
        } else if (stars[i].z == 2) {
            starColor = 0x88;
        }
        
        pixel->r = starColor;
        pixel->g = starColor;
        pixel->b = starColor;
    }
}

const char* ScrollerScene::renderNextChar(const char* scrollerPtr, uint16_t const& dX, uint16_t const& dY, uint8_t const& offset, uint8_t const& length) {
    int c = *scrollerPtr;
    int charIndex = -1;
    if (c >= 65 && c <= 90) {
        charIndex = c - 65;
    } else if (c == '!') {
        charIndex = 26;
    } else if (c == '.') {
        charIndex = 27;
    } else if (c == '?') {
        charIndex = 28;
    } else if (c == ':') {
        charIndex = 29;
    } else if (c == '*') {
        charIndex = 30;
    } else if (c == '-') {
        charIndex = 31;
    } else if (c == '$') {
        charIndex = 32;
    } else if (c >= 48 && c <= 57) {
        charIndex = c + 6;
    }

    if (charIndex >= 0) {
        int xOffset = (charIndex % (fontBitmapSize / fontCharSize)) * fontCharSize;
        int yOffset = (charIndex / (fontBitmapSize / fontCharSize)) * fontCharSize;

        rasterizer->frame.blit(dX, dY, fontBitmap, xOffset + offset, yOffset, length, fontCharSize);
    }
    return ++scrollerPtr;
}

void ScrollerScene::renderScroller(unsigned long const& elapsed) {
    int16_t offset = ((long) elapsed / 4 - SCHWUBBEL_WIDTH) % scrollerPixelLength;
    const char* scrollerPtr;
    uint16_t currentX = 0;
    if (offset < 0) {
        currentX = -offset;
        scrollerPtr = scrollerText;
    } else {
        scrollerPtr = &scrollerText[offset/fontCharSize];
    }
    uint16_t currentY = SCHWUBBEL_HEIGHT - 34;
    while (currentX < SCHWUBBEL_WIDTH) {
        if (currentX == 0) {
            int16_t remainder = offset % fontCharSize;
            scrollerPtr = renderNextChar(scrollerPtr, currentX, currentY, remainder, fontCharSize - remainder);
            currentX += fontCharSize - remainder;
        } else if (SCHWUBBEL_WIDTH - currentX < fontCharSize) {
            scrollerPtr = renderNextChar(scrollerPtr, currentX, currentY, 0, SCHWUBBEL_WIDTH - currentX);
            currentX += fontCharSize;
        } else {
            scrollerPtr = renderNextChar(scrollerPtr, currentX, currentY, 0, fontCharSize + 0);
            currentX += fontCharSize;
        }
        if (*scrollerPtr == '\0') {
            scrollerPtr = scrollerText;
        }
    }
}

LeVertex ScrollerScene::quadraticBezierCurve(float const& t, LeVertex const& start, LeVertex const& end, LeVertex const& control) {
    // [x,y]=(1–t)^2*P0+2*(1–t)*t*P1+t^2*P2
    float u = 1.0f-t;
    float tt = t*t;
    float uu = u*u;
    
    LeVertex result = start * uu;
    result += control*2.0f*u*t;
    result += end*tt;

    return result;
}

void ScrollerScene::start() {
    LeVertex cameraPosition = LeVertex(0.0f, 0.0f, 3.0f);
    renderer->setViewAngle(LeVertex(0.0f, 0.0f, 0.0f));
    renderer->setViewPosition(cameraPosition);
	renderer->updateViewMatrix();
}

void ScrollerScene::stop() {
}

void ScrollerScene::run(unsigned long const& elapsed) {
    if ((elapsed > 40000 && elapsed < 40500) ||
        (elapsed > 42000 && elapsed < 42500) ||
        (elapsed > 43500 && elapsed < 46000)
    ) {
        return;
    }
    
    if (elapsed > 47000) {
        return;
    }

    float vampireTime = elapsed - 37000.f;
    if (vampireTime >= 0.0f) {
        float time = vampireTime/10000.f;
        if (time <= 1.0f) {
            LeVertex start(17.0f, 5.0f, -10.0f);
            LeVertex end(3.5f, -0.5f, 2.0f);
            LeVertex control(-20.0f, 7.0f, -4.0f);

            vampire->pos = quadraticBezierCurve(time, start, end, control);
            vampire->updateMatrix();
        }
        renderer->setBackculling(false);
        renderer->render(vampire);
        renderer->setBackculling(true);
    }

    renderStarField(elapsed);

    rasterizer->frame.rect(0, 0, SCHWUBBEL_WIDTH, barHeight-1, LeColor::rgb(0x636bff));
    rasterizer->frame.rect(0, barHeight-1, SCHWUBBEL_WIDTH, 1, LeColor::rgb(0x4ccfa7));

    rasterizer->frame.rect(0, barHeight + starCanvasHeight, SCHWUBBEL_WIDTH, 1, LeColor::rgb(0x4ccfa7));
    rasterizer->frame.rect(0, barHeight + starCanvasHeight + 1, SCHWUBBEL_WIDTH, barHeight - 1, LeColor::rgb(0x636bff));

    renderScroller(elapsed);


    float felapsed = (float) elapsed;
    float sinSpace = (starCanvasHeight - logo->ty - 10) / 2;
    int32_t targetY = static_cast<int32_t>(sinf(felapsed/1000.0f) * sinSpace) + starCanvasHeight/2 - logo->ty / 2 + starCanvasStart();
    rasterizer->frame.alphaBlit(SCHWUBBEL_WIDTH/2 - logo->tx/2, targetY, logo, 0, 0, logo->tx, logo->ty);


    if (elapsed > 30000 && elapsed < 35000) {
        rasterizer->frame.alphaBlit(384 - sinf((elapsed-20000.f)/800.f) * schnappsGirls->tx, 20, schnappsGirls, 0, 0, schnappsGirls->tx, schnappsGirls->ty);
    }
    
    if (elapsed > 38000) {
        glitch.glitch(elapsed, &rasterizer->frame);
    }
}