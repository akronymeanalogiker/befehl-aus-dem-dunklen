#pragma once

#include "Scene.h"

#include "le3d/engine/bitmap.h"
#include "le3d/engine/rasterizer.h"

#include <memory>

class OutroScene: public Scene {
public:
    OutroScene(LeRasterizer*);

    virtual void run(unsigned long const&) override;
    virtual void postprocess() override;

private:
    LeRasterizer* rasterizer;

    LeBitmap* outro;
    float fadeFactor;
};