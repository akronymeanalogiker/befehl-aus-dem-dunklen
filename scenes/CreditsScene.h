#pragma once

#include "Scene.h"

#include "le3d/engine/bitmap.h"
#include "le3d/engine/rasterizer.h"
#include "util/BezierSegment.h"

#include <memory>
#include <vector>

#define NUM_CREDITS_PATHS 4

class CreditsScene: public Scene {
public:
    CreditsScene(LeRenderer*, LeRasterizer*, std::vector<BezierSegment>[NUM_CREDITS_PATHS]);
    ~CreditsScene();

    virtual void start() override;
    virtual void stop() override;
    virtual void run(unsigned long const&) override;
    virtual void postprocess() override;

private:
    LeVertex cubicBezierCurve(float const&, LeVertex const&, LeVertex const&, LeVertex const&, LeVertex const&);
    void updateCamera(BezierSegment const&, float t);

private:
    int durationPerPath;
    LeRenderer* renderer;
    LeRasterizer* rasterizer;

    std::vector<BezierSegment> cameraPath[NUM_CREDITS_PATHS];
    float cameraPathLength[NUM_CREDITS_PATHS];
    float cameraPathPerMs[NUM_CREDITS_PATHS];

    LeMesh* egg;
    LeMesh* plane;
    LeMesh* plasmaPlane;
    
    int planeTextureSlot;
    int plasmaTextureSlot;

    LeBitmap* planeTexture;
    float fadeFactor;

    LeBitmap* bg;
    LeBitmap* mop;
    LeBitmap* hoshi;
    LeBitmap* virgill;
    LeBitmap plasma;

    int pathIndex;

    LeColor palette[255];
};