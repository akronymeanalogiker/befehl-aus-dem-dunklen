#include "CityScene.h"

#include "util/MathHelpers.h"

CityScene::CityScene(LeRenderer* renderer, LeRasterizer* rasterizer, BezierSegment const& bloodPath) :
    renderer(renderer),
    rasterizer(rasterizer),
    city(nullptr),
    vampire(nullptr),
    godzilla(nullptr),
    bg(nullptr),
    rendereredBackground(),
    bloodPath(bloodPath),
    viewPosition(7.167f, 1.083f, 4.75f),
    viewAngle(13.5f, 28.5f, 0.0f),
    startLookAt(),
    endLookAt(),
    hasFinal(false)
{
    int slot;
    slot = meshCache.getSlotFromName("city.obj");
	city = meshCache.cacheSlots[slot].mesh;

    slot = meshCache.getSlotFromName("vampire.obj");
	vampire = meshCache.cacheSlots[slot].mesh;

    slot = meshCache.getSlotFromName("godzilla.obj");
	godzilla = meshCache.cacheSlots[slot].mesh;

    slot = meshCache.getSlotFromName("street.obj");
	street = meshCache.cacheSlots[slot].mesh;

    slot = meshCache.getSlotFromName("compointermediate.obj");
	compointermediate = meshCache.cacheSlots[slot].mesh;
    compointermediate->scale.x = 0.4f;
    compointermediate->scale.y = 0.4f;
    compointermediate->scale.z = 0.4f;
    compointermediate->angle.y = 180.f;
    compointermediate->pos.x = 7.0001f;
    compointermediate->pos.y = 0.1f;
    compointermediate->pos.z = 2.1f;    
    compointermediate->updateMatrix();

    slot = bmpCache.getSlotFromName("compointermediate_final.bmp");
    compoIntermediateFinal = bmpCache.cacheSlots[slot].bitmap;

    slot = bmpCache.getSlotFromName("bg.bmp");
	bg = bmpCache.cacheSlots[slot].bitmap;

    renderer->setViewPosition(viewPosition);
    renderer->setViewAngle(viewAngle);
    renderer->updateViewMatrix();

    // helpless workaround to find current direction vector
    LeMatrix viewMatrix;
    viewMatrix.identity();
    viewMatrix.translate(-viewPosition);
    viewMatrix.rotate(-viewAngle*d2r);

    LeVertex lookDirection = {
        viewMatrix.mat[2][0],
        viewMatrix.mat[2][1],
        viewMatrix.mat[2][2]
    };

    startLookAt.x = viewPosition.x - lookDirection.x * 6.0f;
    startLookAt.y = viewPosition.y - lookDirection.y * 6.0f;
    startLookAt.z = viewPosition.z - lookDirection.z * 6.0f;

    endLookAt = compointermediate->pos;

    rendereredBackground.allocate(rasterizer->frame.tx, rasterizer->frame.ty);
}

CityScene::~CityScene() {
    rendereredBackground.deallocate();
}

void CityScene::renderEnvironment(unsigned long const& elapsed) {
    if (elapsed > STOMP_START_TIME + STOMP_TIME) {
        renderBackground();
    } else {
        rasterizer->frame.blit(0, 0, &rendereredBackground, 0, 0, rasterizer->frame.tx, rasterizer->frame.ty);
    }
}

void CityScene::renderBackground() {
    rasterizer->frame.blit(0, 0, bg, 0, 0, bg->tx, bg->ty);
    renderer->render(street);
    renderer->render(city);
    renderer->render(compointermediate);
}

void CityScene::start() {
    hasFinal = false;
    renderer->setViewPosition(viewPosition);
    renderer->setViewAngle(viewAngle);
    renderer->updateViewMatrix();

    void* framebuffer = rasterizer->frame.data;

    renderer->flush();
    rasterizer->frame.data = rendereredBackground.data;
    rasterizer->resetPixels();
    rasterizer->flush();
    renderBackground();
    rasterizer->rasterList(renderer->getTriangleList());
    renderer->flush();

    rasterizer->frame.data = framebuffer;
    rasterizer->resetPixels();
    rasterizer->flush();

    godzilla->pos.z = 2.1f;
    godzilla->pos.x = 7.0f;
    godzilla->angle.x = 0.f;
    godzilla->angle.y = -90.f;
    godzilla->angle.z = 0.f;
    godzilla->updateMatrix();

    vampire->angle.x = 0.f;
    vampire->angle.y = 90.f;
    vampire->angle.z = 0.f;
    vampire->scale.x = 0.5f;
    vampire->scale.y = 0.5f;
    vampire->scale.z = 0.5f;
}

void CityScene::run(unsigned long const& elapsed) {

    vampire->pos.x = -5.f + elapsed / 1000.f;
    vampire->pos.y = 0.8f;
    vampire->pos.z = 2.5f;

    unsigned long endStompTime = STOMP_START_TIME + STOMP_TIME;
    unsigned long endShatterTime = endStompTime + SHATTER_TIME;
    unsigned long endLeaveTime = LEAVE_START_TIME + LEAVE_TIME;
    unsigned long endZoomTime = ZOOM_START_TIME + ZOOM_TIME;
    
    if (elapsed > ZOOM_START_TIME && elapsed <= endZoomTime) {
        float t = (elapsed - ZOOM_START_TIME) / (float) ZOOM_TIME;
        LeVertex lookAt = startLookAt + (endLookAt - startLookAt) * t;
        LeVertex cameraPosition = MathHelpers::cubicCurvePosition(bloodPath, t);

        renderer->setViewMatrix(MathHelpers::createLookAtMatrix(cameraPosition, lookAt));
    } else if (elapsed > ZOOM_START_TIME && !hasFinal) {
        LeVertex cameraPosition = MathHelpers::cubicCurvePosition(bloodPath, 1.0);
        renderer->setViewPosition(cameraPosition);
        renderer->setViewAngle(LeVertex(-90.f, 90.f, 0.f));
        renderer->updateViewMatrix();
        hasFinal = true;
    }

    renderEnvironment(elapsed);

    static bool camera_resetted = false;
    if (elapsed >= STOMP_START_TIME && elapsed <= endLeaveTime) {
        if (elapsed > endStompTime) {
            if (elapsed < endShatterTime) {
                float elapsedShatter = elapsed - endStompTime;
                LeVertex cameraPosition = viewPosition;
                cameraPosition.y += sinf((elapsedShatter / (float) SHATTER_TIME) * M_PI * 2);
                renderer->setViewPosition(cameraPosition);
                renderer->updateViewMatrix();
            } else {
                camera_resetted = true;
                renderer->setViewPosition(viewPosition);
                renderer->updateViewMatrix();
            }
            bool updateMatrix = false;
            if (godzilla->pos.y != 0.f) {
                godzilla->pos.y = 0.f;
                updateMatrix = true;
            }

            if (elapsed >= LEAVE_START_TIME) {
                float factor = (elapsed - LEAVE_START_TIME) / (float) LEAVE_TIME;
                godzilla->angle.z = 45.f * factor;
                godzilla->pos.y = 5.f * factor;
                updateMatrix = true;
            }
            if (updateMatrix) {
                godzilla->updateMatrix();
            }
        } else {
            float factor = (elapsed - STOMP_START_TIME) / (float) STOMP_TIME;
            godzilla->pos.y = 3.f - factor * 3.f;
            if (factor > 0.5f) {
                vampire->pos.y = factor * 1.6f;
            }
            godzilla->updateMatrix();
        }
        renderer->render(godzilla);
    }
    if (elapsed < endStompTime) {
        vampire->updateMatrix();
        renderer->setBackculling(false);
        renderer->render(vampire);
        renderer->setBackculling(true);
    }
}

void CityScene::postprocess() {
    if (hasFinal) {
        rasterizer->frame.blit(0, 0, compoIntermediateFinal, 0, 0, compoIntermediateFinal->tx, compoIntermediateFinal->ty);
    }
}