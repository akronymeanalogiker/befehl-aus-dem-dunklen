#include "OutroScene.h"

#include "schwubbel_config.h"
#include "util/BitmapHelpers.h"

OutroScene::OutroScene(LeRasterizer* rasterizer) :
    rasterizer(rasterizer)
{
    int outroSlot = bmpCache.getSlotFromName("outro.bmp");
	outro = bmpCache.cacheSlots[outroSlot].bitmap;
}

void OutroScene::run(unsigned long const& elapsed) {
    rasterizer->frame.blit(0, 0, outro, 0, 0, 384, 216);
    
    float t = elapsed / (float) OUTRO_DURATION;
    fadeFactor = -1.f;
    if (t <= 0.2f) {
        fadeFactor = t/0.2f;
    } else if (t >= 0.8f) {
        fadeFactor = (1.0f-t)/0.2f;
    }    
}

void OutroScene::postprocess() {
    if (fadeFactor >= 0.f) {
        BitmapHelpers::darken(&rasterizer->frame, (uint8_t) (fadeFactor*255.f));
    }
}