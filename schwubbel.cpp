#include "postprocess/Glitch.h"
#include "scenes/CityScene.h"
#include "scenes/CreditsScene.h"
#include "scenes/OutroScene.h"
#include "scenes/ScrollerScene.h"
#include "scenes/SkyTreeScene.h"
#include "DebugWindow.h"
#include "LoadingScreen.h"
#include "schwubbel_config.h"
#include "schwubbel_support.h"
#include "util/BezierPathParser.h"

#include "le3d/engine/le3d.h"

#include <memory>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#define USE_CHIPMEM 1000000

#ifdef AMIGA
#include <proto/exec.h>
extern "C" {
    void play_init(void*);
    void play_end();
}
#endif

/*****************************************************************************/
int main()
{
    sys.initialize();
/** Create application objects */
#if SCHWUBBEL_DEBUG
    int width = SCHWUBBEL_WIDTH * 3;
    int height = SCHWUBBEL_HEIGHT * 3;
#else
    int width = SCHWUBBEL_WIDTH;
    int height = SCHWUBBEL_HEIGHT;
#endif

    // der esel nennt sich selbst zuerst
    Result<std::vector<BezierSegment>> mopPathResult = BezierPathParser::parseFile("data/mop.json");
    if (!mopPathResult.err.empty()) {
        printf("mop %s\n", mopPathResult.err.c_str());
        sys.terminate();
        return 1;
    }

    Result<std::vector<BezierSegment>> hoshiPathResult = BezierPathParser::parseFile("data/hoshi.json");
    if (!hoshiPathResult.err.empty()) {
        printf("hoshi %s\n", hoshiPathResult.err.c_str());
        sys.terminate();
        return 1;
    }

    Result<std::vector<BezierSegment>> virgillPathResult = BezierPathParser::parseFile("data/virgill.json");
    if (!virgillPathResult.err.empty()) {
        printf("virgill %s\n", virgillPathResult.err.c_str());
        sys.terminate();
        return 1;
    }

    Result<std::vector<BezierSegment>> eggdetailPathResult = BezierPathParser::parseFile("data/eggdetail.json");
    if (!eggdetailPathResult.err.empty()) {
        printf("eggdetail %s\n", eggdetailPathResult.err.c_str());
        sys.terminate();
        return 1;
    }

    Result<std::vector<BezierSegment>> bloodPathResult = BezierPathParser::parseFile("data/blood.json");
    if (!bloodPathResult.err.empty()) {
        printf("blood %s\n", bloodPathResult.err.c_str());
        sys.terminate();
        return 1;
    }
    
    LeWindow	 window		= LeWindow("Le3d: schwubbel-evoke2018", width, height);
    LeDraw		 draw		= LeDraw(window.getContext(), width, height);

    LeRenderer	 renderer	= LeRenderer(SCHWUBBEL_WIDTH, SCHWUBBEL_HEIGHT);
    renderer.setViewClipping(0.1f, LE_RENDERER_FAR_DEFAULT);

    LeRasterizer rasterizer = LeRasterizer(SCHWUBBEL_WIDTH, SCHWUBBEL_HEIGHT);
    LeColor black = LeColor::rgb(0x000000);
    rasterizer.background = black;

#ifdef AMIGA
    const char* path = "audio/p61.schwubbel.mod";
    FILE * file = fopen(path, "rb");
	if (!file) {
		printf("file not found %s!\n", path);
		return 2;
	}

    void* audio = AllocMem(USE_CHIPMEM, MEMF_CHIP);
    if (audio == 0) {
        printf("Failed allocating chipmem\n");
        return 1;
    }

    int size;
    fseek(file, 0L, SEEK_END);
    int filelength = ftell(file);
    rewind(file);

    void* offset = ((char*) audio + 100000);

    fread(offset, 1, filelength, file);
    fclose(file);
#endif

    LoadingScreen loadingScreen(&rasterizer, &window, &draw);
    loadingScreen.load();

/** Set the renderer properties */
/** Initialize the timing */
    init_timing();

    LeBitmap off1;
    off1.allocate(SCHWUBBEL_WIDTH, SCHWUBBEL_HEIGHT + 2);
    LeBitmap off2;
    off2.allocate(SCHWUBBEL_WIDTH, SCHWUBBEL_HEIGHT + 2);

    LeBitmap* fb2 = &off1;
    LeBitmap* fb3 = &off2;

    int vampireSlot = meshCache.getSlotFromName("vampire.obj");
    LeMesh* vampire = meshCache.cacheSlots[vampireSlot].mesh;

    std::vector<BezierSegment> paths[] = {
        mopPathResult.v,
        hoshiPathResult.v,
        virgillPathResult.v,
        eggdetailPathResult.v
    };
    CityScene cityScene(&renderer, &rasterizer, bloodPathResult.v[0]);
    CreditsScene creditsScene(&renderer, &rasterizer, paths);
    ScrollerScene scrollerScene(&renderer, &rasterizer, vampire);
    SkyTreeScene skyTreeScene(&renderer, &rasterizer, vampire);
    OutroScene outroScene(&rasterizer);

#if SCHWUBBEL_DEBUG
    LeBitmap drawBitmap;
    drawBitmap.allocate(width, height + 2);
    
    DebugWindow debugWindow(width, height);
    window.registerKeyCallback(debugWindow.keyCallback);
    window.registerMouseCallback(debugWindow.mouseCallback);
    
    DebugSettings& debugSettings = debugWindow.getSettings();
    DebugScene lastDebugScene = DebugScene::FULL_DEMO;
    debugSettings.cityViewAngle = cityScene.getViewAngle();
    debugSettings.cityViewPosition = cityScene.getViewPosition();
#endif
    Scene* lastScene = NULL;

#ifdef AMIGA
    play_init(offset);
#endif
    unsigned long start = millis();
    while (sys.running && window.visible) {
        // clear everything
        rasterizer.flush();
        unsigned long now = millis();
        unsigned long elapsed = now - start;

    /** Draw the triangles */
        
        unsigned long sceneElapsed = 0;
        Scene* scene = NULL;

        if ((sceneElapsed = elapsed) < SCROLLER_DURATION) {
            scene = &scrollerScene;
        } else if ((sceneElapsed = elapsed - SCROLLER_DURATION) < SKYTREE_DURATION) {
            scene = &skyTreeScene;
        } else if ((sceneElapsed = elapsed - SCROLLER_DURATION - SKYTREE_DURATION) < CITY_DURATION) {
            scene = &cityScene;
        } else if ((sceneElapsed = elapsed - SCROLLER_DURATION - SKYTREE_DURATION - CITY_DURATION) < CREDITS_DURATION) {
            scene = &creditsScene;
        } else if ((sceneElapsed = elapsed - SCROLLER_DURATION - SKYTREE_DURATION - CITY_DURATION - CREDITS_DURATION) < OUTRO_DURATION) {
            scene = &outroScene;
        }

#if SCHWUBBEL_DEBUG
        if (lastDebugScene != debugSettings.debugScene) {
            lastDebugScene = debugSettings.debugScene;
            if (lastDebugScene == DebugScene::FULL_DEMO) {
                scene = &scrollerScene;
            }
            start = now;
            sceneElapsed = 0;
        }
        switch(lastDebugScene) {
            case DebugScene::FULL_DEMO:
                // handled above
                break;
            case DebugScene::SCROLLER_SCENE:
                scene = &scrollerScene;
                break;
            case DebugScene::SKYTREE_SCENE:
                scene = &skyTreeScene;
                break;
            case DebugScene::CITY_SCENE:
                scene = &cityScene;
                break;
            case DebugScene::CREDITS_SCENE:
                scene = &creditsScene;
                break;
            case DebugScene::OUTRO_SCENE:
                scene = &outroScene;
                break;
        }
#endif
        if (scene) {
            if (scene != lastScene) {
                if (lastScene) {
                    scene->stop();
                }
                scene->start();
            }
            scene->run(sceneElapsed);
        } else {
            sys.terminate();
        }


        rasterizer.rasterList(renderer.getTriangleList());
        renderer.flush();
        if (scene) {
            scene->postprocess();
        }

        lastScene = scene;

        const void* pixelData;
#if SCHWUBBEL_DEBUG
        debugSettings.elapsed = elapsed;
        debugSettings = debugWindow.update();
        if (debugSettings.elapsed != elapsed) {
            start = now - debugSettings.elapsed;
        }
        if (scene == &cityScene) {
            cityScene.setViewAngle(debugSettings.cityViewAngle);
            cityScene.setViewPosition(debugSettings.cityViewPosition);
        }
        drawBitmap.clear(black);
        drawBitmap.alphaScaleBlit(0, 0, width, height, &rasterizer.frame, 0, 0, SCHWUBBEL_WIDTH, SCHWUBBEL_HEIGHT);
        LeColor* s = (LeColor*) (debugWindow.getBitmap())->data;
        LeColor* d = (LeColor*) drawBitmap.data;
        for (int i=0;i<width*height;i++) {
            if (s[i].a == 0) {
                d[i] = s[i];
            }
        }
        pixelData = drawBitmap.data;
#else
        pixelData = rasterizer.getPixels();
#endif
        // wait_next_frame();
        draw.setPixels(pixelData);
        void* tmp = rasterizer.frame.data;
        rasterizer.frame.data = fb2->data;
        fb2->data = fb3->data;
        fb3->data = tmp;
        rasterizer.resetPixels();

        sys.update();
        window.update();
    }
#ifdef AMIGA
    play_end();
    FreeMem(audio, USE_CHIPMEM);
#endif
    cleanup_timing();

    sys.terminate();
    return 0;
}