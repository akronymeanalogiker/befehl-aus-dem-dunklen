#pragma once

#include "DebugSettings.h"

#include "le3d/engine/bitmap.h"

#include "imgui-1.62/imgui.h"
#include "imgui-sw-renderer/imgui_sw.hpp"

class DebugWindow {
public:
    DebugWindow(uint16_t const& width, uint16_t const& height);
    virtual ~DebugWindow();
    
    DebugSettings& update();
    LeBitmap* getBitmap() {
        return &bitmap;
    }

public:
    static void keyCallback(int key, int state);
	static void mouseCallback(int x, int y, int buttons);

public:
    DebugSettings& getSettings() {
        return this->settings;
    }

private:
    struct DebugSettings settings;
    uint16_t width;
    uint16_t height;
    LeBitmap bitmap;
    imgui_sw::SwOptions swOptions;
};