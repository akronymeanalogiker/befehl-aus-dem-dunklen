#pragma once

#include "le3d/engine/le3d.h"

enum DebugScene {
    FULL_DEMO,
    SCROLLER_SCENE,
    SKYTREE_SCENE,
    CITY_SCENE,
    CREDITS_SCENE,
    OUTRO_SCENE
};

struct DebugSettings {
    DebugScene debugScene;
    unsigned long elapsed;
    LeVertex cityViewPosition;
    LeVertex cityViewAngle;
};