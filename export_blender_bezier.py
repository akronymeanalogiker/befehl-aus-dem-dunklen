import json
import bpy

def bezier_point(t, start, control_1, control_2, end):
    return  start * (1.0 - t) * (1.0 - t)  * (1.0 - t) + 3.0 *  control_1 * (1.0 - t) * (1.0 - t)  * t             + 3.0 *  control_2 * (1.0 - t) * t          * t             +              end * t         * t          * t

def bezier_length(start, c1, c2, end):
    length = 0.0
    steps = 100
    previous_dot = None
    for i in range(0, steps + 1):
        t = i / steps
        dot = {
            "x": bezier_point(t, start.x, c1.x, c2.x, end.x),
            "y": bezier_point(t, start.y, c1.y, c2.y, end.y),
        }
        if previous_dot is not None:
            x_diff = dot["x"] - previous_dot["x"]
            y_diff = dot["y"] - previous_dot["y"]
            length += sqrt (x_diff * x_diff + y_diff * y_diff)
        previous_dot = dot
    return length

def exportSegment(start, c1, c2, end):
    return {
        'sx': start.x,
        'sy': start.y,
        'sz': start.z,
        'c1x': c1.x,
        'c1y': c1.y,
        'c1z': c1.z,
        'c2x': c2.x,
        'c2y': c2.y,
        'c2z': c2.z,
        'ex': end.x,
        'ey': end.y,
        'ez': end.z,
        'len': bezier_length(start, c1, c2, end),
    }

curve = "blood"
myCurve = bpy.data.objects[curve] # here your curve
spline= myCurve.data.splines[0] # maybe you need a loop if more than 1 spline


print("\n======================")
points = []
start = spline.bezier_points[0]
for x in range(1, len(spline.bezier_points)):
    end = spline.bezier_points[x]
    points.append(exportSegment(start.co, start.handle_right, end.handle_left, end.co))
    start = end

text_file = open("/Users/mop/projects/schwubbel-evoke2018/data/" + curve + ".json", "w")
text_file.write(json.dumps(points, sort_keys=True, indent=2, separators=(',', ': ')))
text_file.close()

